<?php

namespace Test\FizzBuzz;

use FizzBuzz\Program;
use PHPUnit\Framework\TestCase;

/**
 * Class ProgramTest
 * @package Test\FizzBuzz
 */
class ProgramTest extends TestCase
{
    /**
     */
    public function setUp()
    {
        parent::setUp();

        self::displayAllErrors();
    }

    /**
     */
    public function test_result_3_and_5()
    {
        $result = (new Program)->run(100);
        foreach ($result as $index => $value) {
            $three = $index % 3 === 0;
            $five = $index % 5 === 0;
            if ($three && $five) {
                $this->assertEquals('Fizz Buzz', $value);
            }
        }
    }

    /**
     */
    public function test_result_3()
    {
        $result = (new Program)->run(100);
        foreach ($result as $index => $value) {
            $three = $index % 3 === 0;
            $five = $index % 5 === 0;
            if ($three && $five) {
                continue;
            }
            if ($three) {
                $this->assertEquals('Fizz', $value);
            }
        }
    }

    /**
     */
    public function test_result_5()
    {
        $result = (new Program)->run(100);
        foreach ($result as $index => $value) {
            $three = $index % 3 === 0;
            $five = $index % 5 === 0;
            if ($three && $five) {
                continue;
            }
            if ($five) {
                $this->assertEquals('Buzz', $value);
            }
        }
    }

    /**
     */
    private static function displayAllErrors(): void
    {
        error_reporting(E_ALL);
        ini_set('display_errors', 1);
    }
}
