<?php

namespace FizzBuzz;

/**
 * Class Program
 */
class Program
{
    /**
     * @param int $max
     * @return array
     */
    public function run(int $max): array
    {
        $answers = [];
        for ($counter = 1; $counter <= $max; $counter++) {
            $three = $counter % 3 === 0;
            $five = $counter % 5 === 0;
            if ($three && $five) {
                $answers[$counter] = 'Fizz Buzz';
                $this->output($answers[$counter]);
                continue;
            }
            if ($three) {
                $answers[$counter] = 'Fizz';
                $this->output($answers[$counter]);
                continue;
            }
            if ($five) {
                $answers[$counter] = 'Buzz';
                $this->output($answers[$counter]);
                continue;
            }
            $answers[$counter] = $counter;
            $this->output($counter);
        }
        return $answers;
    }

    /**
     * @param $value
     */
    protected function output(string $value)
    {
        echo "{$value}", PHP_EOL;
    }
}
