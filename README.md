# Task description

Write a program that prints all numbers from 1 to 100.
 - for those multiple of 3, print "Fizz" instead of the number.
 - for those multiple of 5, print "Buzz" instead of the number.
 - for those multiple of both 3 and 5, print "Fizz Buzz" instead of the number.
